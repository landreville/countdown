
const vertexShader = `
varying vec3 vNormal;
attribute float displacement;

void main() {
  vNormal = normal;
  vec3 newPos = position + normal * vec3(displacement);
  
  gl_Position = projectionMatrix *
                modelViewMatrix *
                vec4(newPos, 1.0);
}
`;


const fragmentShader = `
varying vec3 vNormal;

void main() {
  vec3 light = vec3(0.5, 0.2, 1.0);
  light = normalize(light);
  float dProd = max(0.2, dot(vNormal, light));
  
  gl_FragColor = vec4(dProd,  // R
                      dProd,  // G
                      dProd,  // B
                      1.0); // A
}
`;

function main() {
  let scene = new THREE.Scene();
  let camera = new THREE.PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    0.1,
    1000
  );
  let attributes = {
    displacement: {
      type: 'f',
      value: []
    }
  };

  let renderer = new THREE.WebGLRenderer();
  renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(renderer.domElement);


  let geometry = new THREE.BoxGeometry(1, 1, 1);
  let material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
  let shaderMaterial = new THREE.ShaderMaterial({
    vertexShader: vertexShader,
    fragmentShader: fragmentShader,
    attributes: attributes
  });
  let cube = new THREE.Mesh(geometry, shaderMaterial);



  let verts = cube.geometry.vertices;
  let values = attributes.displacement.value;
  for (let i = 0; i < verts.length; i++) {
    values.push(Math.random() * 30);
  }

  scene.add(cube);
  camera.position.z = 5;

  function animate() {
    requestAnimationFrame(animate);
    renderer.render(scene, camera);
    cube.rotation.x += 0.01;
    cube.rotation.y += 0.01;
  }

  animate();
}

main();
